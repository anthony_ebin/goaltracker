$(function()
{
    var $next = $("#wizard_next_button");
    var $prev = $("#wizard_prev_button");
    var $goalNameTextBox = $("#wizard_goal_name");
    var $goalTargetTextBox = $("#wizard_goal_target_date");
    var $goalWizardForm = $(".goal_wizard form");
    var $newGoalSubmitButton = $("#new_goal_submit");  
    var $goalDetailLines = $(".goal_detail_line");  
    var $tasksLists = $(".tasks_list");
    var $taskWizards = $(".task_wizard");
    var $weekDaySelect = $(".week_days");
    var $monthOptions = $(".month_options");

    // hide certain fields on start
    hideStartFields();
    
    function hideStartFields()
    {
        $goalTargetTextBox.hide();
        $newGoalSubmitButton.hide();
        $goalDetailLines.hide();
        $taskWizards.hide();
        $tasksLists.hide();
        $weekDaySelect.hide();
        $monthOptions.hide();
    }
    
    $next.click(function()
    {
        // hide goal name and show goal target date
        if (!$goalNameTextBox.is(":hidden")) {
            animateTransition($goalNameTextBox, $goalTargetTextBox, function(){$goalTargetTextBox.focus()});
        }
        else if (!$goalTargetTextBox.is(":hidden")) {
            $goalWizardForm.submit();
        }
    });
    
    $prev.click(function()
    {
        // hide goal target date and show goal name 
        if ($goalNameTextBox.is(":hidden")) {
            animateTransition($goalTargetTextBox, $goalNameTextBox, function(){$goalNameTextBox.focus()});
        }
    });
    
    // animate a transition from element 1 to 2 and do something on completion if given
    function animateTransition($from, $to, onComplete)
    {
        onComplete = onComplete || "";
        
        $from.slideUp(500, function(){
            $to.slideDown(500, onComplete);
        });
    }

    // prevent default event when enter is clicked on goal
    $(".goal_wizard").keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return $next.click();
        }
    });
    
    // when goal line is clicked, show tasks and hide other goals
    var $goalLine = $(".goal_line");

    $goalLine.click(function()
    {
        //factory reset :) otherwise when we show task form it will stay on even when user clicks on goal line
        $taskWizards.hide();
        
        var $otherGoalLines = $(".goal_line").not($(this));
        var $thisGoalDetails = $(this).parent().children(".goal_detail_line");
        var $thisGoalTasks = $(this).parent().children(".tasks_list");
        
        var $thisGoalDetailsAndTasks = $thisGoalDetails.add($thisGoalTasks);
        
        // if other goals are available but hidden then hide this goal and show other goals
        if ($otherGoalLines.length) {
            if ($otherGoalLines.is(":hidden")) {
                animateTransition($thisGoalDetailsAndTasks, $otherGoalLines)
            } else {
                animateTransition($otherGoalLines, $thisGoalDetailsAndTasks)
            }            
        } else $thisGoalDetailsAndTasks.slideToggle(500);
        
    });
    
    // when add task is clicked, show new task form
    var $btnAddTask = $(".btn_add_task");
    
    $btnAddTask.click(function() {
       var $frmAddTask = $(this).parent().parent().parent().children(".task_wizard");

       var $thisGoalTasks = $(this).parent().parent().parent().children(".tasks_list");

       animateTransition($thisGoalTasks, $frmAddTask); 
       
    });
    
    var $goalWizard = $(".goal_wizard");
    
    // if goals exists then by default hide the goal wizard unless add goal is clicked
    if ($goalLine.length) {
        $goalWizard.hide();
    } else $goalWizard.show();
    
    
    $("#burger").click(function() {
        //hide title
        if ($(".title_text").is(":hidden")) {
            animateTransition($(".title_menu_item"), $(".title_text"));
        } else
        {
            animateTransition($(".title_text"), $(".title_menu_item"));
        }

    });
    
    $("#btn_add_goal").click(function() {
       $goalWizard.toggle(500,function(){
           $goalNameTextBox.focus();

       });
            animateTransition($(".title_menu_item"), $(".title_text"));
       
    });
    
    
    $(".delete_goal_form").click(function(event) {
       event.preventDefault();
       
       if (confirm("Are you sure? This action cannot be reversed.")) {
           $(this).submit();
       }
    });
    
    // calling datepicker tp code
    $(".datepicker").datepicker();
    
    //show interval field only if there is a repeat value
    $("#task_freq").change(function(){
       if ($(this).val()!="single") {
            $("#task_interval").slideDown(500);
            if ($(this).val()=="weekly") {
                // change placeholder for weekly
                $("#task_interval").prop("placeholder", "repeat every x weeks");
                //add week day selector
                $weekDaySelect.slideDown(500);
            } else $weekDaySelect.slideUp(500);
            if ($(this).val()=="monthly") {
                $("#task_interval").prop("placeholder", "repeat every x months");
                $monthOptions.slideDown(500);
            } else $monthOptions.slideUp(500);
       } else{
          $("#task_interval").slideUp(500);
          $weekDaySelect.slideUp(500);
          $monthOptions.slideUp(500);
       }  
    });
    
    $(".week_days input[type=checkbox]").change(function() {
        if($(this).is(":checked")) {
            $(this).parent().addClass("day_selected");
        } else {
            $(this).parent().removeClass("day_selected");
        }
    });
    

    // render charts
    try {
       /* code */
        callCharts();
    } catch (e) {
       console.log('error in try catch of callcharts: ' + e.message)
    }

    // render correlation
    var data = new Array(
                jsvarsnamespace.grades,
                new Array(20,54,54,65,45) //put goal grades here
    );
    alert(pearsonCorrelation(data,0,1))

    
});

function callCharts()
{
   
    //Charts
    var data = {
      // A labels array that can contain any sort of values
      labels: jsvarsnamespace.labels,
      // Our series array that contains series objects or in this case series data arrays
      series: [jsvarsnamespace.grades]
    };
    
    new Chartist.Line('.ct-chart', data);
    
    // Initialize a Line chart in the container with the ID chart2
    new Chartist.Bar('#chart2', {
        labels: [1, 2, 3, 4],
        series: [[5, 2, 8, 3]]
    });    
}

/**
 *  Calculate the person correlation score between two items in a dataset.
 *
 *  @param  {object}  prefs The dataset containing data about both items that
 *                    are being compared.
 *  @param  {string}  p1 Item one for comparison.
 *  @param  {string}  p2 Item two for comparison.
 *  @return {float}  The pearson correlation score.
 */
function pearsonCorrelation(prefs, p1, p2) {
  var si = [];

  for (var key in prefs[p1]) {
    if (prefs[p2][key]) si.push(key);
  }

  var n = si.length;

  if (n == 0) return 0;

  var sum1 = 0;
  for (var i = 0; i < si.length; i++) sum1 += prefs[p1][si[i]];

  var sum2 = 0;
  for (var i = 0; i < si.length; i++) sum2 += prefs[p2][si[i]];

  var sum1Sq = 0;
  for (var i = 0; i < si.length; i++) {
    sum1Sq += Math.pow(prefs[p1][si[i]], 2);
  }

  var sum2Sq = 0;
  for (var i = 0; i < si.length; i++) {
    sum2Sq += Math.pow(prefs[p2][si[i]], 2);
  }

  var pSum = 0;
  for (var i = 0; i < si.length; i++) {
    pSum += prefs[p1][si[i]] * prefs[p2][si[i]];
  }

  var num = pSum - (sum1 * sum2 / n);
  var den = Math.sqrt((sum1Sq - Math.pow(sum1, 2) / n) *
      (sum2Sq - Math.pow(sum2, 2) / n));

  if (den == 0) return 0;

  return num / den;
}
//# sourceMappingURL=all.js.map
