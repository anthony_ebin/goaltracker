@extends('layouts.app')

@section('header_scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.1/fullcalendar.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.1/fullcalendar.min.css"/>
    <script src="/rrule.js"></script>
    <script src="/nlp.js"></script>
    
@endsection

@section('content')
    <!--Set Id so that we can access the id from other js functions-->
    {!! $calendar->setId('main')->calendar() !!}
    {!! $calendar->script() !!}
@endsection

@section('footer_scripts')
        <script>
        $(document).ready(function() {

            // alert('test');
             $('#calendar-main').fullCalendar( 'addEventSource',        
                function(start, end, status, callback) {

                    //converting to datetime objects
                    start = new Date(start);
                    end = new Date(end);
                    
                    // When requested, dynamically generate a repeatable event for every sunday.
                    var events = [];
                    var sunday = 0;
                    var one_day = (24 * 60 * 60 * 1000);
                    
                    // get each event in current calendar
                    var parentEvents = $('#calendar-main').fullCalendar('clientEvents');
                    
                    // for each event in parentEvents generate array of events from recur details of event
                    for (var key in parentEvents)
                    {
                        evFreq = parentEvents[key].freq;
                        if(evFreq){
                            evInterval = parentEvents[key].interval;
                            evTitle = parentEvents[key].title;
                            evStartDate = new Date(parentEvents[key].start);
                            // parse "until" date as proper date
                            evUntilDate = parentEvents[key].until;
                            evUntilDate = evUntilDate.replace(/ /, "T"); 
                            evUntilDate = new Date(evUntilDate);
                            // alert(evUntilDate);
                            
                            var ruleObj = {};
                            // default is daily
                            ruleObj['freq'] = evFreq=='daily'?RRule.DAILY:'daily';
                            ruleObj['interval'] = evInterval;
                            ruleObj['dtstart'] = evStartDate;
                            ruleObj['until'] = evUntilDate;
                            
                            // Create a rule using the rrule.js lib - https://github.com/jkbrzt/rrule
                            var rule = new RRule(ruleObj);
                                // alert(rule);
                            
                            var recurDates = rule.all();
                            
                            for (key in recurDates)
                            {
                                //add to events array for each datetime in rule.all()
                                events.push({
                                    title: evTitle,
                                    start: recurDates[key],
                                    allDay: false
                                });
                            }
                            
                        }
                    }

                    // return events generated
                    callback(events);
                }
            );
        });
    </script>
@endsection
