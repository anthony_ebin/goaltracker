<div class="title">
    <button><i class="material-icons">face</i></button>
    <h1 class="title_text">OathKeeper</h1>
    <nav class="burger_menu">
        <a id="btn_add_goal" class="title_menu_item" href="goals#wizard_goal_name">Add Goal</a>
        <a class="title_menu_item" href="tasks">Activity Log</a>
        <a class="title_menu_item" href="goal">Goal Planner</a>
        <a class="title_menu_item" href="charts">Diagnostics</a>
        <a class="title_menu_item" href="/">Home Page</a>
        <button id="burger"><i class="material-icons">menu</i></button>
    </nav>
</div>
