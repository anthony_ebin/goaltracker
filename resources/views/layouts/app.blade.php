<!DOCTYPE html>
<html lang="en">
    <head>
        <!--Tell the browser that the page should be sized to the width of the device or browser window itself (not the "desktop" page size)-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Anthony's Goal Tracker</title>

        <!-- CSS And JavaScript -->
        <link rel="stylesheet" href="css/app.css">
        @yield('header_scripts')
        <!--jquery cdn-->
        <script src="https://code.jquery.com/jquery-2.2.2.min.js" integrity="sha256-36cp2Co+/62rEAAYHLmRCPIych47CvdM+uTBJwSzWjI=" crossorigin="anonymous"></script>
        <!--For calendar-->
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <!--my scripts-->
        <script type="text/javascript" src="js/all.js"></script>
        <!--Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Flamenco' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css'>
        <!--Icons https://design.google.com/icons/-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        
    </head>

    <body>
        <div class="container">
            @yield('content')
        </div>
    
        @yield('footer_scripts')
        <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>-->
    </body>
</html>