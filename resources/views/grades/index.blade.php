@extends('layouts.app')

@section('header_scripts')
    <!--For charts-->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
    <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    @include('common.jsvars')
@stop

@section('content')
    <!-- Display Validation Errors -->
    @include('common.errors')
    
    @include('common.title')
    
    <!--Date buttons-->
    <div class="heading" id="charts_heading"><h2>Charts</h2></div>
    
    <!--Charts-->
    <div class="ct-chart ct-perfect-fourth"></div>
    
    <div id="chart2"></div>
    
    
    
@endsection