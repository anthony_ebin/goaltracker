@extends('layouts.app')

@section('content')
    <!-- Display Validation Errors -->
    @include('common.errors')
    
    @include('common.title')
    
    <!--Date buttons-->
    <div class="heading" id="grades_heading">
        <a href="tasks?view=day&start={{Carbon\Carbon::parse(Request::input('start'))->subDay()->toDateString()}}"><i class="material-icons">navigate_before</i></a>
        <h2>Tasks for {{ Carbon\Carbon::parse(Request::input('start'))->format('l, jS \\of F Y')}}</h2>
        <a href="tasks?view=day&start={{Carbon\Carbon::parse(Request::input('start'))->addDay()->toDateString()}}"><i class="material-icons">navigate_next</i></a>
    </div>
    
    <form id="grades_form" method="POST" action="{{ url('grade') }}">
        {!! csrf_field() !!}
    @foreach($events as $event)
        <div class="grade_event">
            <label>{{ $event['title'] }}</label>
            <input type="number" name="{{ $event['eventType'] }}_grade[{{$event['eventID'] }}]" value="{{ isset($event['grade'])?$event['grade']:0 }}" min="0" max="100" />
        </div>
    @endforeach
        <input type="text" hidden name="viewType" value="{{ Request::input('view')}}"/>
        <input type="text" hidden name="dueDate" value="{{ Request::input('start')}}"/>
        <input type="submit" value="Update"/>
    </form>

@endsection