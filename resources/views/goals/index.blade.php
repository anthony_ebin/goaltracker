@extends('layouts.app')

@section('content')
    <!-- Display Validation Errors -->
    @include('common.errors')

    @include('common.title')

    <!-- progressbar -->
    <div class="goal_wizard">
        <!-- New goal Form -->
        <form action="{{ url('goal') }}" method="POST">
            {!! csrf_field() !!}
            <!-- goal Name -->
            <button type="button" id="wizard_prev_button" class="wizard_prev_button"><i class="material-icons">navigate_before</i></button>
            <input type="text" id="wizard_goal_name" name="goal_name" placeholder="I want to...">
            <input type="text" id="wizard_goal_target_date" name="goal_target_date" placeholder="I will do this by...">
            <button type="button" id="wizard_next_button" class="wizard_next_button"><i class="material-icons">navigate_next</i></button>
    
            <!-- Add goal Button -->
            <button type="submit" id="new_goal_submit">Add Goal</button>
        </form>
	</div>

    <div class="heading"><h2>Current Goals</h2></div>

    <div class="goal_list">
        <!-- Current goals -->
        @if (count($goals) > 0)
            @foreach ($goals as $goal)
                <div class="goal_full_line">
                    <div class="goal_line">
                        <div class="goal_item">{{ str_limit($goal->name,75) }}</div>
                        <div class="goal_target_date">Due: {{ $goal->target_date->toDateString() }}</div>
                    </div>
                    <div class="goal_detail_line">
                        <div class="goal_description">{{ $goal->description }}</div>
                        <div class="goal_options">
                            <!--Add Task-->
                            <button class="btn_add_task"><i class="material-icons">add</i></button>
                            <!--Edit Goal-->
                            <button class="btn_edit_goal"><i class="material-icons">mode_edit</i></button>
                            <!--Delete goal-->
                            <form class="delete_goal_form" action="{{ url('goal/'.$goal->id) }}" method="POST">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <button class="btn_delete_goal"><i class="material-icons">delete_forever</i></button>
                            </form>
                        </div>
                    </div>
                    <div class="tasks_list">
                @if (count($goal->tasks()) > 0)
                    @foreach ($goal->tasks()->get() as $task)
                        <div class="task_line">
                            <div class="task_name">Task: {{ $task->name }}</div>
                            <div class="task_description">Description: {{ $task->description }}</div>
                            <div class="task_due">Due Date: {{ $task->pivot->until }}</div>
                            <div class="task_recur">Recurring?: {{ $task->pivot->freq!='single'?'Yes':'No' }}</div>
                        </div>
                    @endforeach
                @endif
                    </div>
                    <div class="task_wizard">
                        <h2>Adding a new task</h2>
                        <!--add task to goal-->
                        <form action="{{ url('task') }}" method="POST">
                            {!! csrf_field() !!}
                            <input type="text" name="task_name" id="{{ $goal->id }}_task" placeholder="To {{ $goal->name }}, I will...">
                            <input id="task_description" name="task_description" type="text" placeholder="optional description"/>
                            <input id="task_start_date" name="task_start_date" class="datepicker" type="text"  placeholder="start date"/>
                            <select id="task_freq" name="task_freq" type="text" placeholder="frequency">
                                <option value="single" selected="selected">Don't repeat</option>
                                <option value="daily">Repeat daily</option>
                                <option value="weekly">Repeat weekly</option>
                                <option value="monthly">Repeat monthly</option>
                            </select>
                            <input id="task_interval" name="task_interval" type="number" placeholder="repeat every x days" min="1" max="99" hidden/>
                            <div class="week_days">
                                <label><input type="checkbox" name="byday" value="SU">Sun</label>
                                <label class="day_selected"><input type="checkbox" name="byday" value="MO" checked>Mon</label>
                                <label><input type="checkbox" name="byday" value="TU">Tue</label>
                                <label><input type="checkbox" name="byday" value="WE">Wed</label>
                                <label><input type="checkbox" name="byday" value="TH">Thu</label>
                                <label class="day_selected"><input type="checkbox" name="byday" value="FR" checked>Fri</label>
                                <label><input type="checkbox" name="byday" value="SA">Sat</label>
                            </div>
                            <div class="month_options">
                                <label><input type="radio" name="month_option_radio" val="byday" checked/>on day</label>
                                <input name="month_option_byday" id="month_option_byday" type="number" min="1" max="31" value="1"/>
                                <label><input type="radio" name="month_option_radio" val="bydayofweek"/>on the</label>
                                <select name="weeknumber">
                                    <option value="1">First</option>
                                    <option value="2">Second</option>
                                    <option value="3">Third</option>
                                    <option value="4">Fourth</option>
                                    <option value="-1">Last</option>
                                </select>
                                <select name="weekday_name">
                                    <option value="SU">Sunday</option>
                                    <option value="MO">Monday</option>
                                    <option value="TU">Tuesday</option>
                                    <option value="WE">Wednesday</option>
                                    <option value="TH">Thursday</option>
                                    <option value="FR">Friday</option>
                                    <option value="SA">Saturday</option>
                                </select>
                            </div>
                            <input id="task_until" name="task_until" type="text" class="datepicker" placeholder="until date"/>
                            
                            <input type="hidden" name="goal_id" value="{{ $goal->id }}">   
                            <!-- Add goal Button -->
                            <button type="submit">Add Task</button>
                        </form>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
@endsection