<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoalTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //http://stackoverflow.com/questions/1054201/ical-field-list-for-database-schema-based-on-ical-standard/1397019#1397019
        Schema::create('goal_task', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('goal_id')->unsigned();
            $table->integer('task_id')->unsigned();
            $table->dateTime('start_datetime');
            //this is for duration of the task
            $table->dateTime('end_datetime');
            //freq 'yearly','monthly','weekly','daily','hourly','minutely','single' || NOT NULL
            $table->string('freq', 8)->default('daily');
            //until DateTime Null
            $table->dateTime('until');
            //interval Integer Not Null Default(1)
            $table->integer('interval')->default(1);
            //byMinute varChar(170) Null E.g. Every 20 minutes from 9:00 AM to 4:40 PM every day: RRULE:FREQ=DAILY;BYHOUR=9,10,11,12,13,14,15,16;BYMINUTE=0,20,40
            $table->string('byMinute', 170);
            //byHour varChar(61) Null
            $table->string('byHour', 170);
            //byDay varChar(35) Null E.g. Every day in January: RRULE:FREQ=YEARLY;BYMONTH=1;BYDAY=SU,MO,TU,WE,TH,FR,SA OR RRULE:FREQ=DAILY;BYMONTH=1
            $table->string('byDay', 170);
            //byMonthDay varChar(200) Null
            $table->string('byMonthDay', 170);
            //byYearDay varChar(3078) Null
            $table->string('byYearDay', 170);
            //byWeekNo varChar(353) Null
            $table->string('byWeekNo', 170);
            //byMonth varChar(29) Null
            $table->string('byMonth', 170);
            $table->timestamps();
            
            $table->foreign('goal_id')->references('id')->on('goals')->onDelete('cascade');
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
		Schema::table('goal_task', function(Blueprint $table)
		{
			$table->dropForeign('goal_task_goal_id_foreign');
			$table->dropForeign('goal_task_task_id_foreign');
		});
		
        Schema::drop('goal_task');
    }
}
