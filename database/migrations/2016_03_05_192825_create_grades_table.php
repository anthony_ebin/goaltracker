<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grades', function (Blueprint $table) {
            $table->increments('id');
            //The gradeable_id column will contain the ID value of the goal or task
            $table->integer('gradeable_id')->unsigned();
            //The gradeable_type column will contain the class name of the owning model, i.e. Goal or Task. 
            //The gradeable_type column is how the ORM determines which "type" of owning model to return when accessing the gradeable relation.
            $table->string('gradeable_type');
            $table->dateTime('due_date');
            $table->tinyInteger('percent_completed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('grades');
    }
}
