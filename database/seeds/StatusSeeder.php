<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusSeeder extends Seeder {

    public function run()
    {
        DB::table('statuses')->delete();

        if (null == Status::first())
        {
            Status::create(['name' => 'Active']);
            Status::create(['name' => 'Completed']);
            Status::create(['name' => 'Dropped']);
        }

    }

}