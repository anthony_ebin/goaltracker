# OathKeeper

## Description
Oathkeeper was an idea I worked on to help organize one’s goals and habits. Built using Laravel and Javascript, I intended it to help me manage my projects effectively and identify lapses in execution.

## Screenshots
![Imgur](https://i.imgur.com/t7SiSnAl.png)
![Imgur](https://i.imgur.com/Isx9ENEl.png)
![Imgur](https://i.imgur.com/VQAd7hGl.png)
![Imgur](https://i.imgur.com/L3avWHZl.png)
![Imgur](https://i.imgur.com/tAtQxpWl.png)