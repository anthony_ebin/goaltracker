<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['due_date', 'percent_completed'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['due_date'];
    
    /**
     * Get all of the owning gradeable models, Goal and Task.
     */
    public function gradeable()
    {
        return $this->morphTo();
    }
}
