<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Goal;
use App\Status;
use App\Repositories\GoalRepository;
use Carbon\Carbon;

class GoalController extends Controller
{
    /**
     * The goal repository instance.
     *
     * @var GoalRepository
     */
    protected $goals;

    /**
     * Create a new controller instance.
     *
     * @param  TaskRepository  $tasks
     * @return void
     */
    public function __construct(GoalRepository $goals)
    {
        $this->middleware('auth');

        $this->goals = $goals;
    }

    /**
     * Display a list of all of the user's task.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        return view('goals.index', [
            'goals' => $this->goals->forUser($request->user()),
        ]);
        
        // from tinker
        // $user = User::first();
        // $goal = $user->goals()->create(['status_id'=>1, 'name'=>'tester']);
        // $goal->tasks()->create(['name'=>'eat well']);
    }
    
    /**
     * Create a new goal.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'goal_name' => 'required|max:255',
        ]);
    
        $request->user()->goals()->create([
            'name' => $request->goal_name,
            'start_date' => Carbon::now(),
            'target_date' => $request->goal_target_date,
            'status_id'=>Status::whereName('Active')->first()->id
        ]);
    
        return redirect('/goals');
    }
    
    /**
     * Destroy the given goal.
     *
     * @param  Request  $request
     * @param  Goal  $goal
     * @return Response
     */
    public function destroy(Request $request, Goal $goal)
    {
        $this->authorize('destroy', $goal);
        
        $goal->delete();

        return redirect('/goals');
    }

    
}
