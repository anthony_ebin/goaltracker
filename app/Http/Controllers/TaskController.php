<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Task;
use App\Goal;
use Calendar;
use Auth;
use Carbon\Carbon;

class TaskController extends Controller
{
 
    
    /**
     * Display a calendar with all the persisted tasks and goals
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all goals and put them in the events array with a special color
        $goals = Auth::user()->goals()->get();
        
        foreach ($goals as $goal) {
            $events[] = Calendar::event(
                        $goal->name, //event title
                        true, //full day event?
                        $goal->target_date, //start time (you can also use Carbon instead of DateTime)
                        $goal->target_date, //end time (you can also use Carbon instead of DateTime)
                        $goal->id, //optionally, you can specify an event ID
                        [
                            // you can add url here
                            'color' => '#800'
                        ]
                    );
            
            // get all goal_tasks and put them in the events array with a different color
            $goal_tasks = $goal->tasks()->get();
                
                foreach ($goal_tasks as $goal_task) {
                    // $until = $goal_task->pivot->until;
                    // $until = $until instanceof DateTime ? $until : new DateTime($until);
                    $events[] = Calendar::event(
                        $goal_task->name, //event title
                        false, //full day event?
                        $goal_task->start_datetime, //start time (you can also use Carbon instead of DateTime)
                        $goal_task->end_datetime, //end time (you can also use Carbon instead of DateTime)
                        $goal_task->id, //optionally, you can specify an event ID
                        [
                            'color' => '#395',
                            'freq' => $goal_task->pivot->freq,
                            'until' => $goal_task->pivot->until,
                            'interval' => $goal_task->pivot->interval
                        ]
                    );
                }
        }
        
        // $goal_tasks = 
        // $task = Goal::latest()->first()->tasks()->first();
        // $taskDet = $task->pivot;
        
        // dd($task->name);
        
        $calendar = Calendar::addEvents($events) //add an array with addEvents
            ->setOptions([ //set fullcalendar options
                'firstDay' => 1
            ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
                'viewRender' => 'function() {}'
            ]);
            
        return view('calendar', compact('calendar'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'task_name' => 'required|max:255',
        ]);
        
        // check if task already exists in db, if yes use that to add to the goal_tasks, if not create task then add it to the goal
        $task = Task::whereName($request->task_name)->first();
        
        if(!isset($task))
        {
            $task = $request->user()->tasks()->create([
                'name' => $request->task_name,
                'description' => $request->task_description
            ]);
        }
        // update task description if task is already in db
        else {
            $task->description=$request->task_description;
            $task->save();
        }
        
        $parsedStartDate = Carbon::parse($request->task_start_date);
        $parsedUntilDate = Carbon::parse($request->task_until);
        
        Goal::find($request->goal_id)->tasks()->attach($task->id,[
                                                'start_datetime'=>$parsedStartDate,
                                                'end_datetime'=>$parsedStartDate,
                                                'freq'=>$request->task_freq,
                                                'until'=>$parsedUntilDate,
                                                'interval'=>$request->task_interval]);
        
        return redirect('/goals');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
