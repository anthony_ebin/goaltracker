<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DateTime;
use DateTimeZone;
use Auth;
use App\Task;
use App\Grade;
use JavaScript;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // https://github.com/laracasts/PHP-Vars-To-Js-Transformer
        $gradesObjects = Grade::all();
        $grades = $gradesObjects->lists('percent_completed');
        $labels = $gradesObjects->lists('due_date');
    
        foreach ($labels as $key=>$value) {
            $labels[$key] = $value->toDateString();
        }
            // dd($labels);

        
        JavaScript::put([
            'labels' => $labels,
            'grades' => $grades
        ]);
     
        return view('grades.index', compact('grades'));   
    }

    /**
     * Display a view with all the persisted tasks and 
     * e.g. of query: view=day&start=2016-03-13
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $viewType = $request->view;
        // parse start date
        $viewStartDate = new DateTime($request->start);
        // end date is one day plus start date if day view
        $viewEndDate = new DateTime($request->start);
        $viewEndDate = $viewType=='day'?$viewEndDate->modify('+1 day'):$request->endDate;
        // initialize events array
        $events = array();

        // per day view of tasks
        if ($viewType=='day')
        {
            // get list of goal events where startdatetime is in this period and store in events array
            $goals = Auth::user()->goals()->whereDate('target_date', '>=', $viewStartDate)->get();
            foreach ($goals as $goal) 
            {
                //getting grade if available
                $grade = $goal->grades()->whereDate('due_date', '=', $viewStartDate)->first();
                $grade = isset($grade)?$grade->percent_completed:null;
                $event = ['title' => $goal->name,
                           'start_datetime' => $goal->target_date,
                           'end_datetime' => $goal->target_date,
                           'color' => '#800',
                           'eventID' => $goal->id,
                           'eventType'=> 'goal',
                           'recur' => false,
                           'grade'=>isset($grade)?$grade:null];
                // push event to array
                array_push($events, $event);
            }
            
            
            // get list of goal_task non recur events where startdatetime is in this period and store in events array
            $goal_tasks = Auth::user()->tasks()->join('goal_task', 'goal_task.task_id', '=', 'tasks.id')
                            ->where('freq', 'single')
                            ->whereDate('start_datetime', '=', $viewStartDate)
                            ->get();
            
            foreach ($goal_tasks as $goal_task)
            {
                // only one grade per non-recur task so get one record
                $grade = $goal_task->grades()->first();
                $grade = isset($grade)?$grade->percent_completed:null;
                
                // only show non recur task if it is incomplete
                if ($grade<100) 
                {
                    // check if task is already in events array, don't reinsert
                    $duplicateTask = false;
                    foreach ($events as $key=>$value) {
                        if ($value['eventID']==$goal_task->id&&$value['eventType']=='task') {
                            $duplicateTask=true;
                            break;
                        }
                    }
                    
                    if (!$duplicateTask) 
                    {
                        $event = ['title' => $goal_task->name,
                                   'start_datetime' => $goal_task->start_datetime,
                                   'end_datetime' => $goal_task->end_datetime,
                                   'color' => '#395',
                                   'eventID' => $goal_task->id,
                                   'eventType'=> 'task',
                                   'recur' => false,
                                   'grade'=>isset($grade)?$grade:null];
                        
                        array_push($events, $event);
                    }
                }
            }
            
            // use rrule parse to get a list of events in current time period from recur goal_task
            
            // get list of goal_tasks with recurring data
            $goal_tasks = Task::join('goal_task', 'goal_task.task_id', '=', 'tasks.id')
                ->where('freq', '<>', 'single')->get();
            // for each goal_task create a rrule array of dates within timeframe of current period - https://github.com/simshaun/recurr 
            foreach($goal_tasks as $goal_task)
            {
                // check if task is already in events array, don't reinsert
                $duplicateTask = false;
                foreach ($events as $key=>$value) {
                    if ($value['eventID']==$goal_task->id&&$value['eventType']=='task') {
                        $duplicateTask=true;
                        break;
                    }
                }
                
                if (!$duplicateTask) 
                {

                    // if goal_task until date is before view start date then don't use this goal_task
                    if (new DateTime($goal_task->until) > $viewStartDate) {
                        $timezone    = 'America/New_York';
                        
                        $startDate   = new DateTime($goal_task->start_datetime, new DateTimeZone($timezone));
                        $endDate     = new DateTime($goal_task->start_datetime, new DateTimeZone($timezone));
                        $rule        = new \Recurr\Rule('FREQ=DAILY;UNTIL='.$goal_task->until, $startDate, $endDate, $timezone);
                        $transformer = new \Recurr\Transformer\ArrayTransformer();
                        
                        //filter rrule to get dates collection for this day only
                        $rruleDateCollection = $transformer->transform($rule)->startsBetween($viewStartDate, $viewEndDate, $inc = false);
                        // print_r($rruleDateCollection);
    
                        $grade = $goal_task->grades()->whereDate('due_date', '=', $viewStartDate)->first();
                        $grade = isset($grade)?$grade->percent_completed:null;
                        // if rrule array contains dates within the view period then add them to the events array
                        foreach ($rruleDateCollection as $date) {
                            $event = ['title' => $goal_task->name,
                               'start_datetime' => $date->getStart(),
                               'end_datetime' => $date->getEnd(),
                               'color' => '#555',
                               'eventID' => $goal_task->id,
                               'eventType'=> 'task',
                               'recur' => true,
                               'grade'=>isset($grade)?$grade:null];
                    
                            array_push($events, $event);
                        }
                        
                    }
                }
            }

            // combine lists together, maybe create an array of events for today
            // pass array to view
            return view('grades.create', compact('events'));

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task_grades = $request->task_grade;
        $goal_grades = $request->goal_grade;
        $viewType = $request->viewType; 
        $dueDate = $request->dueDate;
        
        if (isset($task_grades)) {

            foreach ($task_grades as $key => $value) {
                //if task is non recurring then only update one grade record, don't look at due date
                if(Task::find($key)->goals()->first()->pivot->freq=='single')
                {
                    $grade = Grade::where('gradeable_id', $key)->where('gradeable_type', 'App\Task')->first();
                } else {
                    // else record grade per task per day
                    $grade = Grade::where('gradeable_id', $key)->where('due_date', new DateTime($dueDate))->where('gradeable_type', 'App\Task')->first();   
                }
                
                if (!isset($grade)) {
                    $grade = Grade::create();
                }
                $grade->gradeable_id = $key;
                $grade->gradeable_type = 'App\Task';
                $grade->due_date = $dueDate;
                $grade->percent_completed = $value;
                $grade->save();
            }
        }
        
        if (isset($goal_grades)) {
            foreach ($goal_grades as $key => $value) {
                $grade = Grade::where('gradeable_id', $key)->where('gradeable_type', 'App\Goal')->first();
                if (!isset($grade)) {
                    $grade = Grade::create();
                }
                $grade->gradeable_id = $key;
                $grade->gradeable_type = 'App\Goal';
                $grade->due_date = $dueDate;
                $grade->percent_completed = $value;
                $grade->save();
            }
        }
        
        return redirect("/tasks?view=$viewType&start=$dueDate");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
