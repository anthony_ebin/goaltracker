<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Goal extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'start_date', 'target_date', 'status_id', 'completed_date'];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'start_date', 'target_date', 'completed_date'];
    
    /**
     * Mutator to convert target_date in input to a Carbon instance
     *
     * @param $date
     */
    public function setTargetDateAttribute($date)
    {
        $this->attributes['target_date'] = Carbon::parse($date);
    }
    
    /**
     * Get the user that owns the task.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
     
   /**
     * Get the tasks that belong to this goal.
     */
    public function tasks()
    {
        return $this->belongsToMany(Task::class)->withPivot('start_datetime', 'end_datetime', 'freq', 'until', 'interval', 'id');
    }
    
    /**
     * Get all of the goal's grades.
     */
    public function grades()
    {
        return $this->morphMany('App\Grade', 'gradeable');
    }
    
}
