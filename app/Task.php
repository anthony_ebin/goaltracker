<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'name', 'description'];
    
    /**
     * Get the user that owns the task.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
   /**
     * Get the goals that belong to this task.
     */
    public function goals()
    {
        return $this->belongsToMany(Goal::class)->withPivot('start_datetime', 'end_datetime', 'freq', 'until', 'interval', 'id');
    }
    
    /**
     * Get all of the task's grades.
     */
    public function grades()
    {
        return $this->morphMany('App\Grade', 'gradeable');
    }
}
