<?php

use App\Goal;
use App\Task;
use App\Grade;
use Carbon\Carbon;

class GoalTest extends TestCase
{

    /**
     * A basic timed goal
     *
     * @return void
     */
    public function testBasicTimedGoal()
    {
        $this->registerDummy('testUser');
        
        $testStartDate = Carbon::now();
        $testTargetGoalDate = $testStartDate->copy()->addDays(20);
        $testTask1StartDate = $testStartDate->copy()->addDay();
        $testTask2StartDate = $testStartDate->copy()->addDays(2);
        
        // I log a goal: porn free.
        $this->visit('/goals')
             ->type('10 days porn free', 'goal_name')
            // I log a goal date as this is a timed goal.
             ->type($testTargetGoalDate->toDateString(),'goal_target_date')
             ->press('Add Goal')
             ->see('10 days porn free')
             ->see($testTargetGoalDate->toDateString());
         
        $goal = Goal::latest()->first();
         
        // I add tasks to this goal
        $this->type('Self caress', 'task_name')
             ->type('Caress yourself like a mother would a baby', 'task_description')
             ->type($testTask1StartDate->toDateString(), 'task_start_date')
             ->select('daily', 'task_freq')
             ->type($testTargetGoalDate->toDateString(), 'task_until')
             ->press('Add Task')
             ->type('Cold showers', 'task_name')
             ->type($testTask2StartDate->toDateString(), 'task_start_date')
             ->select('single', 'task_freq')
             ->type($testTargetGoalDate->toDateString(), 'task_until')
             ->press('Add Task')
             ->see('Self caress')
             ->see('Caress yourself like a mother would a baby')
             ->see('Recurring?: Yes')
             ->see($testTargetGoalDate->toDateString())
             ->see('Cold showers');
        
        //store goal tasks in variables to access id later     
        $goal_task_1 = $goal->tasks()->get()->all()[0]->pivot;
        $goal_task_2 = $goal->tasks()->get()->all()[1]->pivot;
        
        // dd($goal_task_2);
        
        // visit route for the logged day e.g. /tasks?view=day&start=2016-03-07 you should see both tasks self caress and cold showers
        //  task 1 is recurring so should be visible till end date
        $this->visit('/tasks?view=day&start='.$testTask2StartDate->toDateString())
        // compare expected list of tasks with what appears in view
             ->see('Self caress')
             ->see('Cold showers')
            // you should see a slider, or text box to log accomplishment per task
             ->type('50', 'task_grade['.$goal_task_1->id.']')
            // log an accomplishment e.g. 50% on self caress, hit update
             ->press('Update')
            // you should see the 50% figure from earlier, change it to 70% and hit update
             ->see('50')
             ->type('70', 'task_grade['.$goal_task_1->id.']')
             ->press('Update')
            // you should see 70%
             ->see('70');
             
        // task 2 is non recurring so should be visible only on start date which is also the due date of a non-recurring task
        
        $task2day2 = $testTask2StartDate->copy()->addDay();
        
        // visit day 1 of task 2, it should be visible
        $this->visit('/tasks?view=day&start='.$testTask2StartDate->toDateString())
            ->see('Cold showers')
        // visit day 2 of task 2, it should not be visible
            ->visit('/tasks?view=day&start='.$task2day2->toDateString())
            ->dontSee('Cold showers')
        // visit due date of task 2, it should not be visible
            ->visit('/tasks?view=day&start='.$testTargetGoalDate->toDateString())
            ->dontSee('Cold showers')
        // mark it as 100% complete
            ->visit('/tasks?view=day&start='.$testTask2StartDate->toDateString())
            ->type('100', 'task_grade['.$goal_task_2->id.']')
            ->press('Update')
        // visit day 1,2,3,4 of task 2, it should no longer be visible
            ->visit('/tasks?view=day&start='.$testTask2StartDate->toDateString())
            ->dontSee('Cold showers');
            
            
    }

}
